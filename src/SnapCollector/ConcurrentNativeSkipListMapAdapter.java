package SnapCollector;

import java.util.HashMap;
import java.util.Iterator;

public class ConcurrentNativeSkipListMapAdapter<T>  implements  IterableList<T> {

    private java.util.concurrent.ConcurrentSkipListMap<Integer, T> _inner;
    public ConcurrentNativeSkipListMapAdapter()
    {
        _inner = new java.util.concurrent.ConcurrentSkipListMap<Integer,T>();
    }

    @Override
    public boolean add(T x) {
        return _inner.put(x.hashCode(),x) == null;
    }

    @Override
    public boolean remove(T x) {
        return _inner.remove(x.hashCode()) != null;
    }

    @Override
    public boolean contains(T x) {
        return _inner.containsKey(x.hashCode());
    }

    @Override
    public Iterator<T> takeSnapshot() {
        return null;
    }
}
