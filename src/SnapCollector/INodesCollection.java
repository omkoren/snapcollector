package SnapCollector;

public interface INodesCollection<T> extends Iterable<T>
{
    void AddNode(T node);
    void BlockFurtherNodes();
}
