package SnapCollector;
        import java.util.ArrayList;
        import java.util.Iterator;
        import java.util.LinkedList;
        import java.util.concurrent.atomic.AtomicMarkableReference;

public class NonBlockingLinkedList<T>  implements IterableList<T> {
    private final Node<T> head = new Node<T>(Integer.MIN_VALUE);
    final Node<T> tail = new Node<T>(Integer.MAX_VALUE);

    public NonBlockingLinkedList() {
        while (!head.next.compareAndSet(null, tail, false, false)) ;
    }

    @Override
    public boolean add(T x) {
        int key = x.hashCode();
        boolean splice;
        while (true) {
            // find predecessor and curren entries
            Window<T> window = find(head, key);
            Node<T> pred = window.pred;
            Node<T> curr = window.curr;
            // is the key present?
            if (curr.key == key) {
                return false;
            } else {
                // splice in new node
                Node<T> node = new Node<T>(x);
                node.next = new AtomicMarkableReference(curr, false);
                if (pred.next.compareAndSet(curr, node, false, false)) {
                    return true;
                }
            }
        }
    }
    @Override
    public boolean remove(T x) {
        int key = x.hashCode();
        boolean snip;
        while (true) {
            // find predecessor and curren entries
            Window<T> window = find(head, key);
            Node<T> pred = window.pred;
            Node<T> curr = window.curr;
            // is the key present?
            if (curr.key != key) {
                return false;
            } else {
                // snip out matching node
                Node<T> succ = curr.next.getReference();
                snip = curr.next.attemptMark(succ, true);
                if (!snip) {
                    continue;
                }
                pred.next.compareAndSet(curr, succ, false, false);
                return true;
            }
        }
    }
    @Override
    public boolean contains(T x) {
        int key = x.hashCode();
        // find predecessor and curren entries
        Window<T> window = find(head, key);
        Node<T> pred = window.pred;
        Node<T> curr = window.curr;
        return (curr.key == key);
    }

    @Override
    public Iterator<T> takeSnapshot() {
        return null;
    }


    public final class Node<T> {
        final T value;
        int key = 0;
        public AtomicMarkableReference<Node<T>> next;

        // constructor for sentinel nodes
        public Node(int key) {
            value = null;
            this.key = key;
            next = new AtomicMarkableReference<Node<T>>(null, false);
        }

        public Node(T x) {
            value = x;
            this.key = x.hashCode();
            next = new AtomicMarkableReference<Node<T>>(null, false);
        }
    }

    public final class Window<T> {
        public final Node<T> pred;
        public final Node<T> curr;
        public Window(Node<T> pred, Node<T> curr) {
            this.pred = pred;
            this.curr = curr;
        }
    }

    public Window find(Node head, int key) {
        Node<T> pred = null;
        Node<T> curr = null;
        Node<T> succ = null;

        boolean[] marked = {false}; // is curr marked?
        boolean snip;

        retry:
        while (true) {
            pred = head;
            curr = pred.next.getReference();
            while (true) {
                succ = curr.next.get(marked);
                while (marked[0]) {           // replace curr if marked
                    snip = pred.next.compareAndSet(curr, succ, false, false);
                    if (!snip) {
                        continue retry;
                    }
                    curr = pred.next.getReference();
                    succ = curr.next.get(marked);
                }
                if (curr.key >= key) {
                    return new Window<T>(pred, curr);
                }
                pred = curr;
                curr = succ;
            }
        }
    }
}

