package SnapCollector;

import java.util.HashMap;
import java.util.Iterator;

public class ConcurrentSkipListMapAdapter<T>  implements  IterableList<T> {

    private ConcurrentSkipListMap<Integer,T> _inner;
    public ConcurrentSkipListMapAdapter()
    {
        _inner = new ConcurrentSkipListMap<Integer,T>();
    }

    @Override
    public boolean add(T x) {
        return _inner.put(x.hashCode(),x) == null;
    }

    @Override
    public boolean remove(T x) {
        return _inner.remove(x.hashCode()) != null;
    }

    @Override
    public boolean contains(T x) {
        return _inner.containsKey(x.hashCode());
    }

    @Override
    public Iterator<T> takeSnapshot() {
        ISnapCollector<T> sc = _inner.AcquireSnapCollector();
        _inner.CollectSnapshot(sc);



        HashMap<T,Boolean> map = new HashMap<T,Boolean>();


        for (T node: sc.ReadPointers()) {
            map.put(node,false);
        }

        for (Report<T> report : sc.ReadReports()) {

            if(map.getOrDefault(report.GetNode(),false))
                map.put(report.GetNode(),false);
            else
                map.put(report.GetNode(),report.GetReportType() == ReportType.DELETED);
        }


        return map.entrySet()
                .stream()
                .filter(e -> !e.getValue())
                .map(e -> e.getKey())
                .iterator();
    }
}
