package SnapCollector;
import java.util.Date;

public class TestResult {
    public enum TestThreadAction {
        UNKNOWN,
        ITERATE,
        INSERT,
        CONTAINS,
        DELETE,
        MIXED,
    }

    public int successfulOperations;
    public int failedOperation;
    public int successfulInsertOperations;
    public int successfulContainsOperations;
    public int successfulDeleteOperations;
    public int failedInsertOperations;
    public int failedContainsOperations;
    public int failedDeleteOperations;

    public int numberOfItems;

    public Date startTime;
    public Date endTime;
    public boolean ran;
    public TestThreadAction action;
    public int numberOfThreads;
    public int lowerKey;
    public int upperKey;
    public String dsName;

    public TestResult() {
        this.ran = false;
        this.successfulOperations = 0;
        this.failedOperation = 0;
        this.successfulInsertOperations = 0;
        this.failedInsertOperations = 0;
        this.successfulDeleteOperations = 0;
        this.failedDeleteOperations = 0;
        this.successfulContainsOperations = 0;
        this.failedContainsOperations = 0;
        this.startTime = new Date();
        this.endTime = new Date();
        this.action = TestThreadAction.UNKNOWN;
        this.numberOfThreads = 0;
        this.lowerKey = 0;
        this.upperKey = 0;
        this.dsName = "";
        this.numberOfItems = 0;
    }

    public int TotalOperations() {
        return this.successfulOperations + this.failedOperation;
    }

    public int TotalContains() {
        return this.successfulContainsOperations + this.failedContainsOperations;
    }

    public int TotalInserts() {
        return this.successfulInsertOperations + this.failedInsertOperations;
    }

    public int TotalDeletes() {
        return this.successfulDeleteOperations + this.failedDeleteOperations;
    }

    public double PercentOfContains() {
        return (double) TotalContains() / (double) TotalOperations();
    }

    public double PercentOfInserts() {
        return (double) TotalInserts() / (double) TotalOperations();
    }

    public double PercentOfDeletes() {
        return (double) TotalDeletes() / (double) TotalOperations();
    }

    public int NumberOfThreads() { return numberOfThreads; }

    public int NumberOfItems() { return numberOfItems; }

    public int UpperKey() { return upperKey; }

    public int LowerKey() { return lowerKey; }

    public String DataStructureName() { return dsName; }

    public boolean TestRan() {
        return ran;
    }

    @Override
    public String toString() {
        return "ds: " + dsName + " action: " + action + ", start time: " + startTime + ", end time: " + endTime  + ", successful insert: "
                + successfulInsertOperations + ", successful delete: " + successfulDeleteOperations + ", successful contains: "
                + successfulContainsOperations + ", failed insert: " + failedInsertOperations + ", failed delete: " + failedDeleteOperations
                + ", failed contains: " + failedContainsOperations + ", operations: " + TotalOperations() + ", failed: "
                + failedOperation + ", success: " + successfulOperations + " threads " + numberOfThreads + " lower key " + LowerKey() + " upper key " + upperKey;
    }
}
