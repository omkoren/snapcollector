package SnapCollector;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

public class SnapCollector<TNode> implements ISnapCollector<TNode> {

    private final INodesCollection<TNode> _nodesCollection;

    private final ConcurrentHashMap<Long, IReportsCollection<TNode>> _reportsByThread;

    private volatile boolean _active = true;

    public SnapCollector()
    {
        _nodesCollection = new NodesCollection<TNode>();
        _reportsByThread = new ConcurrentHashMap<>();
    }


    @Override
    public void Deactivate() {
        _active = false;
    }

    @Override
    public boolean IsActive() {
        return _active;
    }


    @Override
    public void AddNode(TNode tNode) {
        if (tNode == null) {
            System.out.println("tNode is null");
        } else {
            _nodesCollection.AddNode(tNode);
        }
    }

    @Override
    public void Report(Report<TNode> report) {
        long threadId = Thread.currentThread().getId();
        IReportsCollection<TNode> reportsList = _reportsByThread.get(threadId);
        if (reportsList == null) {
            // new item - allocate
            reportsList = new ReportsCollection<TNode>();
        }

        // list allocated or already exist - add the report
        reportsList.AddReport(report);
    }

    @Override
    public void BlockFurtherReports() {
        for (IReportsCollection<TNode> r : _reportsByThread.values()) {
            r.BlockFurtherReports();
        }
    }

    @Override
    public void BlockFurtherNodes() {
        _nodesCollection.BlockFurtherNodes();
    }

    @Override
    public Iterable<Report<TNode>> ReadReports() {

        LinkedList<Report<TNode>> all = new  LinkedList<Report<TNode>>();

        for (IReportsCollection<TNode> rlist :  _reportsByThread.values()) {
            for (Report<TNode> report : rlist){
                all.push(report);
            }
        }

        return  all;
    }

    @Override
    public Iterable<TNode> ReadPointers() {
        return _nodesCollection;
    }

}
