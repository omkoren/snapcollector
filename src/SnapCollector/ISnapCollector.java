package SnapCollector;

public interface ISnapCollector<TNode> {

    void Deactivate();
    boolean IsActive();
    void Report(Report<TNode> report);
    void AddNode(TNode node);
    void BlockFurtherReports();
    void BlockFurtherNodes();
    Iterable<Report<TNode>> ReadReports();
    Iterable<TNode> ReadPointers();
}
