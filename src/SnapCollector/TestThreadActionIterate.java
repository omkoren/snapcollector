package SnapCollector;

import java.util.Iterator;

public class TestThreadActionIterate<T> extends TestThread<T> {

    public TestThreadActionIterate(TestThreadConfiguration config,TestCase parent) {
        super(config,parent);
        this.results.action = TestResult.TestThreadAction.ITERATE;
    }

    @Override
    public boolean runAction() {

        try
        {
            Iterator<T> iterator = config.ds.takeSnapshot();

            if(iterator == null)
                return  false;

            while (iterator.hasNext())
            {
                this.results.numberOfItems++;
                iterator.next();
            }

            return true;
        }catch(Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }
}
