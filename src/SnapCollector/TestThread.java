package SnapCollector;

import java.util.Date;

public abstract class TestThread<T> extends Thread {

    protected final TestThreadConfiguration<T> config;
    protected TestResult results;
    private volatile boolean running;

    private TestCase parent;

    public TestThread (TestThreadConfiguration config,TestCase parent) {
        this.config = config;
        this.running = false;
        this.results = new TestResult();
        this.parent = parent;
    }

    public void run() {

        while (!parent.running) { };

        // take start timestamp
        results.startTime = new Date();

        while (parent.running) {
            if(runAction()) {
                results.successfulOperations++;
            } else {
                results.failedOperation++;
            }
        }

        // take end timestamp
        results.endTime = new Date();
    }


    public TestResult GetResults() {
        return results;
    }


    public abstract boolean runAction();
}
