package SnapCollector;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicMarkableReference;
import java.util.concurrent.atomic.AtomicReference;

public class IterableNonBlockingLinkedList<T> implements IterableList<T>{
    private final Node<T> head = new Node<T>(Integer.MIN_VALUE);
    final Node<T> tail = new Node<T>(Integer.MAX_VALUE);
    private AtomicReference<ISnapCollector<T>> _psc;

    public IterableNonBlockingLinkedList() {
        while (!head.next.compareAndSet(null, tail, false, false)) ;

        _psc = new AtomicReference<ISnapCollector<T>>(new SnapCollector<T>());
        _psc.get().Deactivate();
    }

    public boolean add(T x) {
        int key = x.hashCode();
        boolean splice;
        while (true) {
            // find predecessor and curren entries
            Window<T> window = find(head, key);
            Node<T> pred = window.pred;
            Node<T> curr = window.curr;
            // is the key present?
            if (curr.key == key) {
                ReportInsert(curr, window.pred.next.isMarked());
                return false;
            } else {
                // splice in new node
                Node<T> node = new Node<T>(x);
                node.next = new AtomicMarkableReference(curr, false);
                if (pred.next.compareAndSet(curr, node, false, false)) {
                    ReportInsert(node, pred.next.isMarked());
                    return true;
                }
            }
        }
    }

    public boolean remove(T x) {
        int key = x.hashCode();
        boolean snip;
        while (true) {
            // find predecessor and curren entries
            Window<T> window = find(head, key);
            Node<T> pred = window.pred;
            Node<T> curr = window.curr;
            // is the key present?
            if (curr.key != key) {
                return false;
            } else {
                // snip out matching node
                Node<T> succ = curr.next.getReference();
                snip = curr.next.attemptMark(succ, true);
                if (!snip) {
                    continue;
                }
                ReportDelete(succ);
                pred.next.compareAndSet(curr, succ, false, false);
                return true;
            }
        }
    }

    public boolean contains(T x) {
        int key = x.hashCode();
        // find predecessor and curren entries
        Window<T> window = find(head, key);
        Node<T> pred = window.pred;
        Node<T> curr = window.curr;
        if (curr.key == key) {

            if(window.pred.next.isMarked())
            {
                ReportDelete(window.pred.next.getReference());
                return  false;
            }

            ReportInsert(curr, window.pred.next.isMarked());
            return true;
        }
        return false;
    }

    @Override
    public Iterator<T> takeSnapshot() {
        ISnapCollector<T> sc = AcquireSnapCollector();
        CollectSnapshot(sc);

        HashMap<T,Boolean> map = new HashMap<T,Boolean>();


        for (T node: sc.ReadPointers()) {
            map.put(node,false);
        }

        for (Report<T> report : sc.ReadReports()) {

            if(map.getOrDefault(report.GetNode(),false))
                map.put(report.GetNode(),false);
            else
                map.put(report.GetNode(),report.GetReportType() == ReportType.DELETED);
        }


        return map.entrySet()
                .stream()
                .filter(e -> !e.getValue())
                .map(e -> e.getKey())
                .iterator();
    }

    public final class Node<T> {
        final T value;
        int key = 0;
        public AtomicMarkableReference<Node<T>> next;

        // constructor for sentinel nodes
        public Node(int key) {
            value = null;
            this.key = key;
            next = new AtomicMarkableReference<Node<T>>(null, false);
        }

        public Node(T x) {
            value = x;
            this.key = x.hashCode();
            next = new AtomicMarkableReference<Node<T>>(null, false);
        }
    }

    public final class Window<T> {
        public final Node<T> pred;
        public final Node<T> curr;

        public Window(Node<T> pred, Node<T> curr) {
            this.pred = pred;
            this.curr = curr;
        }
    }

    public Window find(Node head, int key) {
        Node<T> pred = null;
        Node<T> curr = null;
        Node<T> succ = null;

        boolean[] marked = {false}; // is curr marked?
        boolean snip;

        retry: while (true) {
            pred = head;
            curr = pred.next.getReference();
            while (true) {
                succ = curr.next.get(marked);
                while (marked[0]) {           // replace curr if marked
                    snip = pred.next.compareAndSet(curr, succ, false, false);
                    if (!snip) {
                        continue retry;
                    }
                    ReportDelete(curr);
                    curr = pred.next.getReference();
                    succ = curr.next.get(marked);
                }
                if (curr.key >= key) {
                    return new Window<T>(pred, curr);
                }
                pred = curr;
                curr = succ;
            }
        }
    }

    public LinkedList<T> getElements() {
        java.util.LinkedList<T> lst = new LinkedList<T>();
        Node<T> curr = this.head.next.getReference();
        while (curr != null) {
            lst.add(curr.value);
            curr = curr.next.getReference();
        }
        lst.removeLast();
        return lst;
    }

    private void ReportInsert(Node<T> newNode, boolean marked)
    {
        ISnapCollector<T> sc = _psc.get();

        if(sc.IsActive() && !marked)
        {
            Report<T> r = new Report<T>(newNode.value,ReportType.INSERTED);
            sc.Report(r);
        }

    }

    private void ReportDelete(Node<T> victim)
    {
        ISnapCollector<T> sc = _psc.get();

        if(sc.IsActive())
        {
            Report<T> r = new Report<T>(victim.value,ReportType.DELETED);
            sc.Report(r);
        }

    }

    private ISnapCollector<T> AcquireSnapCollector()
    {
        ISnapCollector<T> sc = _psc.get();
        if(sc.IsActive())
            return sc;

        ISnapCollector<T> newSc = new SnapCollector<T>();
        _psc.compareAndSet(sc,newSc);
        newSc = _psc.get();
        return  newSc;
    }

    private void CollectSnapshot(ISnapCollector<T> sc)
    {
        boolean[] marked = {false};
        Node<T> curr = head.next.get(marked);

        while (sc.IsActive())
        {
            if(!marked[0]) {
                sc.AddNode(curr.value);
            }

            Node<T> next = curr.next.get(marked);

            // if next is null or we have reached the tail of the list, break
            if(next == null || next == this.tail)
            {
                sc.BlockFurtherNodes();
                sc.Deactivate();
            }
            curr = next;
        }

        sc.BlockFurtherReports();
    }

}

