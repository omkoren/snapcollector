package SnapCollector;

import java.util.Iterator;

public interface IterableList<T>  {
    boolean add(T x);
    boolean remove (T x);
    boolean contains(T x);
    Iterator<T> takeSnapshot();
}
