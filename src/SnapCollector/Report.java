package SnapCollector;

public class Report<T> {
    private final T _node;
    private final ReportType _reportType;

    public Report(T node, ReportType reportType) {
        _node = node;
        _reportType = reportType;
    }

    public T GetNode() {
        return _node;
    }

    public ReportType GetReportType() {
        return _reportType;
    }
}
