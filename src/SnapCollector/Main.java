package SnapCollector;

import javafx.util.Pair;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static org.kohsuke.args4j.ExampleMode.ALL;

public class Main {

    private static Logger logger = Logger.getLogger("Main");

    @Option(name="--repeat",usage="number of times to repeat a test suite", required = true)
    private int numOfRepetitions;

    @Option(name="--threads",usage="number of max threads to run the test cases with", required = true)
    private int maxNumOfThreads;

    @Option(name="--duration",usage="duration in ms for a single test case", required = true)
    private long timeForTest;

    @Option(name="--help", help = true)
    private boolean help;


    private List<Pair<Integer, Integer>> keyRanges =
            Arrays.asList(
                    new Pair<>(1, 32),
                    new Pair<>(1, 128),
                    new Pair<>(1, 1024)
            );


    private static void setupLogger() {
        try {
            File dir = new File("logs");
            if (!dir.exists()) dir.mkdir();
            logger.addHandler(new FileHandler("logs" + File.separator + "SnapCollector.log", true));
            logger.getHandlers()[0].setFormatter(new SimpleFormatter());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runSanity() throws Exception {

        List<IterableList<Integer>> dsList = new LinkedList<IterableList<Integer>>();
        // create ds for sanity

        dsList.add(new IterableNonBlockingLinkedList<Integer>());
        dsList.add(new ConcurrentSkipListMapAdapter<Integer>());
        dsList.add(new IterableLockFreeSkipList<Integer>());

        dsList.add(new NonBlockingLinkedList<Integer>());
        dsList.add(new ConcurrentNativeSkipListMapAdapter<Integer>());
        dsList.add(new LockFreeSkipList<Integer>());

        // create next item random
        Callable<Integer> nextItem = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return ThreadLocalRandom.current().nextInt(0, 99999);
            }
        };

        // for each ds run sanity for single thread and multi threads
        for (IterableList<Integer> ds : dsList) {
            TestSanity<Integer> tsSingleThread = new TestSanity<Integer>(ds, nextItem, logger, 0);
            TestSanity<Integer> tsMultiThreads = new TestSanity<Integer>(ds, nextItem, logger, ThreadLocalRandom.current().nextInt(1, 99));
            tsSingleThread.run();
            tsMultiThreads.run();
        }
    }

    private void runIteratorTestRepetition(int id)
    {
        String testSuiteName = "IteratorTest " + id;
        List<Callable<IterableList<Integer>>> dsList = new LinkedList<Callable<IterableList<Integer>>>();

        // order is important, this will run first on non blocking then on skip list
        dsList.add(() -> new IterableNonBlockingLinkedList<Integer>());
        dsList.add(()-> new ConcurrentSkipListMapAdapter<Integer>());
        dsList.add(() ->new IterableLockFreeSkipList<Integer>());

        TestSuite ts = new TestSuite(testSuiteName, keyRanges, maxNumOfThreads, dsList, timeForTest, 1, logger);
        ts.start();
    }

    private void runNonIteratorTestRepetition(int id)
    {
        String testSuiteName = "NonIteratorTest " + id;
        List<Callable<IterableList<Integer>>> dsList = new LinkedList<Callable<IterableList<Integer>>>();

        // order is important, this will run first on non blocking then on skip list
        dsList.add(()-> new NonBlockingLinkedList<Integer>());
        dsList.add(() -> new ConcurrentNativeSkipListMapAdapter<Integer>());
        dsList.add(() -> new LockFreeSkipList<Integer>());

        TestSuite ts = new TestSuite(testSuiteName, keyRanges, maxNumOfThreads, dsList, timeForTest, 0, logger);
        ts.start();
    }


    private void runIteratorTests()
    {
        for (int i = 0 ; i < numOfRepetitions ; ++i) {
            logger.info("Starting repetition " + i + " of iterator tests");
            runIteratorTestRepetition(i);
            logger.info("End of repetition " + i + " of iterator tests");
        }
    }

    private void runNonIteratorTests()
    {
        for (int i = 0 ; i < numOfRepetitions ; ++i) {
            logger.info("Starting repetition " + i + " of non iterator tests");
            runNonIteratorTestRepetition(i);
            logger.info("End of repetition " + i + " of non iterator tests");
        }
    }

    private void runIteratorCustomTestRepetition(int id)
    {
        String testSuiteName = "IteratorCustomTest " + id;
        List<Callable<IterableList<Integer>>> dsList = new LinkedList<Callable<IterableList<Integer>>>();

        dsList.add(() ->new IterableLockFreeSkipList<Integer>());

        TestSuite ts = new TestSuite(testSuiteName, keyRanges, maxNumOfThreads, dsList, timeForTest, 1, logger);
        ts.start();
    }


    public static  void main(String[] args) throws Exception {
        new Main().doMain(args);
    }

    public void doMain(String[] args) throws Exception {

        CmdLineParser parser = new CmdLineParser(this);


        try {

            parser.parseArgument(args);

        } catch( CmdLineException e ) {

            System.err.println(e.getMessage());
            System.err.println("SnapCollector [options...]");

            parser.printUsage(System.err);
            System.err.println();

            System.err.println("Example: SnapCollector"+ parser.printExample(ALL));

            return;
        }

        setupLogger();

        runSanity();

        runIteratorTests();

        runNonIteratorTests();

    }
}
