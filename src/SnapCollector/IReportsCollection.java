package SnapCollector;

public interface IReportsCollection<T> extends Iterable<Report<T>> {
    void AddReport(Report<T> report);
    void BlockFurtherReports();
}
