package SnapCollector;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;

public class TestCaseResultFile {
    LinkedHashMap<String, Callable<String>> fileFormat;
    private final LinkedList<TestResult> results;
    private TestResult currentResuult;
    private final long id;

    public  TestCaseResultFile(long id, LinkedList<TestResult> results) {
        this.id = id;
        this.results = results;
        this.fileFormat = new LinkedHashMap<>();
        this.currentResuult = null;
        SetupFileFormat();
    }

    public boolean ExportResults() {
        try {
            WriteResults();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private BufferedWriter CreateResultsWriter() throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_hhmmss");
        File dir = new File("tests");
        if (!dir.exists()) dir.mkdir();
        String results_file = "tests" + File.separator + "TestCase_" + this.id + "_results_" + sdf.format(new Date()) + ".csv";
        BufferedWriter out = null;
        FileWriter fstream = new FileWriter(results_file, true);
        out = new BufferedWriter(fstream);
        return out;
    }

    private void SetupFileFormat() {
        this.fileFormat.put("TestCase ID",  new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "" + id;
            }
        });

        this.fileFormat.put("Action", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.action.toString();
            }
        });

        this.fileFormat.put("Start Time", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.startTime.toString();
            }
        });

        this.fileFormat.put("End Time", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.endTime.toString();
            }
        });

        this.fileFormat.put("Total Failures", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.failedOperation + "";
            }
        });

        this.fileFormat.put("Total Successes", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.successfulOperations + "";
            }
        });

        this.fileFormat.put("Contains Successes", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.successfulContainsOperations + "";
            }
        });

        this.fileFormat.put("Inserts Successes", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.successfulInsertOperations + "";
            }
        });

        this.fileFormat.put("Delete Successes", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.successfulDeleteOperations + "";
            }
        });

        this.fileFormat.put("Contains Failures", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.failedContainsOperations + "";
            }
        });

        this.fileFormat.put("Inserts Failures", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.failedInsertOperations + "";
            }
        });

        this.fileFormat.put("Delete Failures", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.failedDeleteOperations + "";
            }
        });

        this.fileFormat.put("Total Operations", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.TotalOperations() + "";
            }
        });

        this.fileFormat.put("Contains Percent", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.PercentOfContains() + "";
            }
        });

        this.fileFormat.put("Inserts Percent", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.PercentOfInserts() + "";
            }
        });

        this.fileFormat.put("Deletes Percent", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.PercentOfDeletes() + "";
            }
        });

        this.fileFormat.put("Number Of Threads", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.NumberOfThreads() + "";
            }
        });

        this.fileFormat.put("Lower Key", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.LowerKey() + "";
            }
        });

        this.fileFormat.put("Upper Key", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.UpperKey() + "";
            }
        });

        this.fileFormat.put("Data Structure", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.DataStructureName() + "";
            }
        });


        this.fileFormat.put("Number of Items", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return currentResuult.NumberOfItems() + "";
            }
        });
    }

    private void WriteResults() throws IOException {
        BufferedWriter resultsWriter = CreateResultsWriter();
        // write headers
        StringBuilder headerLine = new StringBuilder();
        for (Map.Entry<String, Callable<String>> entry : this.fileFormat.entrySet()) {
            headerLine.append(entry.getKey() + ", ");
        }
        resultsWriter.write(headerLine.toString() + System.lineSeparator());
        // iterate over results
        for (TestResult res : this.results) {
            // write each result in a line
            currentResuult = res;
            StringBuilder resultLine = new StringBuilder();
            for (Map.Entry<String, Callable<String>> entry : this.fileFormat.entrySet()) {
                try {
                    String value = entry.getValue().call();
                    resultLine.append( value + ", ");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            resultsWriter.write(resultLine.toString() + System.lineSeparator());
        }
        resultsWriter.close();
    }
}
