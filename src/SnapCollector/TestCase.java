package SnapCollector;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

import static SnapCollector.TestCaseConfiguration.TestCaseConfigurationType.MIXED_THREADS;
import static SnapCollector.TestCaseConfiguration.TestCaseConfigurationType.THREAD_PER_ACTION;

public class TestCase {
    private final long id;
    private final long timeLimitMilli;
    private TestCaseConfiguration config;
    private final IterableList<Integer> ds;
    private LinkedList<TestResult> results;
    private LinkedList<TestThread> threads;
    private Logger logger;
    public volatile boolean running = false;

    public TestCase(long id, IterableList<Integer> ds, long timeLimitMilli, TestCaseConfiguration testCaseConfig, Logger logger) {
        this.id = id;
        this.ds = ds;
        this.timeLimitMilli = timeLimitMilli;
        this.config = testCaseConfig;
        this.logger = logger;
        this.results = new LinkedList<>();
        this.threads = new LinkedList<>();
    }

    public void run() throws IOException {
        Log("Creating threads for id " + id);
        SetupThreads();
        if (config.configType == THREAD_PER_ACTION) {
            Log("Created all threads (iterator " + config.numOfIterators + ", insert " + config.numOfInserts + ", delete " + config.numOfDeletes + ", " +
                    "contains " + config.numOfContains + ", running them");
        } else if (config.configType == MIXED_THREADS) {
            Log("Create all threads (iterator " + config.numOfIterators + ", mixed " + config.numOfWorkers);
        }
        RunThreads();
        Log("Sleeping for " + timeLimitMilli + "ms");
        WaitForTimeout();
        Log("Stopping all threads");
        StopThreads();
        Log("Writing results");
        WriteResults();
        Log("All done");
    }


    private void Log(String message) {
        this.logger.info("TestCase " + this.id + ": " + message);
    }

    private void RunThreads() {
        // run all threads
        for (TestThread<Integer> t : threads) {
            t.start();
        }

        running = true;
    }

    private void SetupThreads() {
        // create config
        TestThreadConfiguration<Integer> testThreadConfig = new TestThreadConfiguration<Integer>(ds);
        // create next item function
        Callable<Integer> nextItem = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return ThreadLocalRandom.current().nextInt(config.lowerKey, config.upperKey + 1);
            }
        };

        // create iterator threads
        for (int i = 0 ; i < config.numOfIterators; ++i) {
            threads.add(new TestThreadActionIterate<Integer>(testThreadConfig,this));
        }

        switch (config.configType) {
            case THREAD_PER_ACTION:
                break;
            case MIXED_THREADS:
                SetupMixedThreads(testThreadConfig, nextItem);
                break;
        }
    }



    private void SetupMixedThreads(TestThreadConfiguration<Integer> testThreadConfig, Callable<Integer> nextItem) {
        // create mixed threads
        for (int i = 0 ; i < config.numOfWorkers ; ++i) {
            threads.add(new TestThreadWorker<Integer>(testThreadConfig, nextItem,this));
        }
    }

    private void WaitForTimeout() {
        // sleep until time limit is over
        try {
            Thread.currentThread().sleep(timeLimitMilli);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void StopThreads() {

        running = false;

        // wait for all threads to join
        for (TestThread<Integer> t : threads) {
            try {
                t.join();
                Log("Thread " + t.GetResults().action + " join done");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void fillCommonResults(TestResult tr) {
        tr.lowerKey = config.lowerKey;
        tr.upperKey = config.upperKey;
        tr.numberOfThreads = config.numOfWorkers;
        tr.dsName = this.ds.getClass().getCanonicalName();
    }

    private void WriteResults() {
        for (TestThread<Integer> t : threads) {
            fillCommonResults(t.GetResults());
            results.add(t.GetResults());
        }
        TestCaseResultFile resultFile = new TestCaseResultFile(id, results);
        resultFile.ExportResults();
    }

    public LinkedList<TestResult> GetResults() {
        return this.results;
    }
}