package SnapCollector;

import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

public class TestThreadWorker<T> extends TestThread {
    private final Callable<T> nextItem;
    private final ThreadLocalRandom randomCurrent;

    public TestThreadWorker(TestThreadConfiguration<T> testThreadConfig, Callable<T> nextItem,TestCase parent) {
        super(testThreadConfig,parent);
        this.nextItem = nextItem;
        this.results.action = TestResult.TestThreadAction.MIXED;
        this.randomCurrent = ThreadLocalRandom.current();
    }

    @Override
    public boolean runAction() {
        boolean  res = false;
        double actionProb = randomCurrent.nextDouble(0, 1);
        if (actionProb > 0.5) {
            // contains
            try {
                res = config.ds.contains(nextItem.call());
                if (res) {
                    results.successfulContainsOperations++;
                } else {
                    results.failedContainsOperations++;
                }
            } catch (java.lang.Exception e) {
                e.printStackTrace();
            }
        } else if (actionProb >= 0.25){
                // insert
                try {
                    res = config.ds.add(nextItem.call());
                    if (res) {
                        results.successfulInsertOperations++;
                    } else {
                        results.failedInsertOperations++;
                    }
                } catch (java.lang.Exception e) {
                    e.printStackTrace();
                }
        } else {
            // delete
            try {
                res = config.ds.remove(nextItem.call());
                if (res) {
                    results.successfulDeleteOperations++;
                } else {
                    results.failedDeleteOperations++;
                }
            } catch (java.lang.Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
