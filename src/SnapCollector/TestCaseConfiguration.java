package SnapCollector;

public class TestCaseConfiguration {

    public enum TestCaseConfigurationType {
        THREAD_PER_ACTION,
        MIXED_THREADS,
    }

    public final int numOfInserts;
    public final int numOfContains;
    public final int numOfDeletes;
    public final int numOfIterators;
    public final int numOfWorkers;
    public final int lowerKey;
    public final int upperKey;
    public final TestCaseConfigurationType configType;

    public TestCaseConfiguration(int numOfInserts, int numOfContains, int numOfDeletes, int numOfIterators, int lowerKey, int upperKey)
    {
        this.numOfInserts = numOfInserts;
        this.numOfContains = numOfContains;
        this.numOfDeletes = numOfDeletes;
        this.numOfIterators = numOfIterators;
        this.lowerKey = lowerKey;
        this.upperKey = upperKey;
        this.numOfWorkers = numOfInserts + numOfContains + numOfDeletes;
        this.configType = TestCaseConfigurationType.THREAD_PER_ACTION;
    }

    public TestCaseConfiguration(int numOfIterators, int numOfWorkers, int lowerKey, int upperKey) {
        this.numOfIterators = numOfIterators;
        this.numOfWorkers = numOfWorkers;
        this.lowerKey = lowerKey;
        this.upperKey = upperKey;
        this.numOfContains = numOfWorkers / 2;
        this.numOfInserts = numOfWorkers / 4;
        this.numOfDeletes = numOfWorkers / 4;
        this.configType = TestCaseConfigurationType.MIXED_THREADS;
    }

    @Override
    public String toString() {
        String str = "action: " + configType;
        switch (configType) {
            case MIXED_THREADS:
                str += " iterators: " + numOfIterators + " workers: " + numOfWorkers + " key range: " + lowerKey
                        + " up to " + upperKey;
                break;
            case THREAD_PER_ACTION:
                str += " iterators: " + numOfIterators + " contains: " + numOfContains + " inserts: " + numOfInserts
                        + " deletes: " + numOfDeletes + " key range: " + lowerKey + " up to " + upperKey;
                break;
        }
        return str;
    }
}
