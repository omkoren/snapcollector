package SnapCollector;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicMarkableReference;
import java.util.concurrent.atomic.AtomicReference;
import SnapCollector.LockFreeSkipList.Node;

public class IterableLockFreeSkipList<T> implements IterableList<T> {

    private static final int MAX_LEVEL = 64;
    private final Node<T> head = new Node<T>(Integer.MIN_VALUE);
    final Node<T> tail = new Node<T>(Integer.MAX_VALUE);
    private AtomicInteger count;

    private AtomicReference<ISnapCollector<T>> _psc;

    public IterableLockFreeSkipList() {
        for (int i = 0; i < head.next.length; i++) {
            head.next[i] = new AtomicMarkableReference<Node<T>>(tail, false);
        }

        count = new AtomicInteger();

        _psc = new AtomicReference<ISnapCollector<T>>(new SnapCollector<T>());
        _psc.get().Deactivate();
    }


    private void ReportInsert(Node<T> newNode,boolean marked)
    {
        ISnapCollector<T> sc = _psc.get();

        if(sc.IsActive() && !marked)
        {
            Report<T> r = new Report<T>(newNode.value,ReportType.INSERTED);
            sc.Report(r);
        }

    }

    private void ReportDelete(Node<T> victim)
    {
        ISnapCollector<T> sc = _psc.get();

        if(sc.IsActive())
        {
            Report<T> r = new Report<T>(victim.value,ReportType.DELETED);
            sc.Report(r);
        }

    }

    private ISnapCollector<T> AcquireSnapCollector()
    {
        ISnapCollector<T> sc = _psc.get();
        if(sc.IsActive())
            return sc;

        ISnapCollector<T> newSc = new SnapCollector<T>();
        _psc.compareAndSet(sc,newSc);
        newSc = _psc.get();
        return  newSc;
    }

    private void CollectSnapshot(ISnapCollector<T> sc)
    {
        boolean[] marked = {false};
        Node<T> curr = head.next[0].get(marked);

        while (sc.IsActive())
        {
            if(!marked[0]) {
                sc.AddNode(curr.value);
            }

            Node<T> next = curr.next[0].get(marked);

            if(next == null || next == this.tail)
            {
                sc.BlockFurtherNodes();
                sc.Deactivate();
            }
            curr = next;
        }

        sc.BlockFurtherReports();
    }

    public boolean add(T x) {
        int topLevel = randomLevel();
        int bottomLevel = 0;
        Node<T>[] preds = new Node[MAX_LEVEL + 1];
        Node<T>[] succs = new Node[MAX_LEVEL + 1];
        while (true) {
            boolean found = find(x, preds, succs);
            if (found) {
                ReportInsert(succs[0],preds[0].next[0].isMarked());
                return false;
            } else {
                Node<T> newNode = new Node(x, topLevel);
                for (int level = bottomLevel; level <= topLevel; level++) {
                    Node<T> succ = succs[level];
                    newNode.next[level].set(succ, false);
                }
                Node<T> pred = preds[bottomLevel];
                Node<T> succ = succs[bottomLevel];
                newNode.next[bottomLevel].set(succ, false);
                if (!pred.next[bottomLevel].compareAndSet(succ, newNode,
                        false, false)) {
                    continue;
                }
                for (int level = bottomLevel + 1; level <= topLevel; level++) {
                    while (true) {
                        pred = preds[level];
                        succ = succs[level];
                        if (pred.next[level].compareAndSet(succ, newNode, false, false))
                            break;
                        find(x, preds, succs);
                    }
                }
                ReportInsert(newNode,pred.next[bottomLevel].isMarked());
                count.getAndIncrement();
                return true;
            }
        }
    }

    public boolean remove(T x) {
        int bottomLevel = 0;
        Node<T>[] preds = new Node[MAX_LEVEL + 1];
        Node<T>[] succs = new Node[MAX_LEVEL + 1];
        Node<T> succ;
        while (true) {
            boolean found = find(x, preds, succs);
            if (!found) {
                return false;
            } else {
                Node<T> nodeToRemove = succs[bottomLevel];
                for (int level = nodeToRemove.topLevel;
                     level >= bottomLevel + 1; level--) {
                    boolean[] marked = {false};
                    succ = nodeToRemove.next[level].get(marked);
                    while (!marked[0]) {
                        boolean snip =nodeToRemove.next[level].attemptMark(succ, true);

                        if(snip)
                            ReportDelete(succ); //report remove if succeeded

                        succ = nodeToRemove.next[level].get(marked);
                    }
                }
                boolean[] marked = {false};
                succ = nodeToRemove.next[bottomLevel].get(marked);
                while (true) {
                    boolean iMarkedIt =
                            nodeToRemove.next[bottomLevel].compareAndSet(succ, succ,
                                    false, true);
                    succ = succs[bottomLevel].next[bottomLevel].get(marked);
                    if (iMarkedIt) {
                        find(x, preds, succs);
                        count.getAndDecrement();
                        return true;
                    } else if (marked[0]) return false;
                }
            }
        }
    }

    private boolean find(T x, Node<T>[] preds, Node<T>[] succs) {
        int bottomLevel = 0;
        int key = x.hashCode();
        boolean[] marked = {false};
        boolean snip;
        Node<T> pred = null, curr = null, succ = null;
        retry:
        while (true) {
            pred = head;
            for (int level = MAX_LEVEL; level >= bottomLevel; level--) {
                curr = pred.next[level].getReference();
                while (true) {
                    succ = curr.next[level].get(marked);
                    while (marked[0]) {

                        ReportDelete(curr); //Before delete we are reporting

                        snip = pred.next[level].compareAndSet(curr, succ,
                                false, false);

                        if (!snip) continue retry;

                        curr = pred.next[level].getReference();
                        succ = curr.next[level].get(marked);
                    }
                    if (curr.key < key) {
                        pred = curr;
                        curr = succ;
                    } else {
                        break;
                    }
                }
                preds[level] = pred;
                succs[level] = curr;
            }
            return (curr.key == key);
        }
    }

    public boolean contains(T x) {
        int bottomLevel = 0;
        int v = x.hashCode();
        boolean[] marked = {false};
        Node<T> pred = head, curr = null, succ = null;

        int level = MAX_LEVEL;
        for (; level >= bottomLevel; level--) {
            curr = pred.next[level].getReference();
            while (true) {
                succ = curr.next[level].get(marked);
                while (marked[0]) {
                    //curr == pred
                    curr = curr.next[level].getReference();
                    succ = curr.next[level].get(marked);
                }
                if (curr.key < v) {
                    pred = curr;
                    curr = succ;
                } else {
                    break;
                }
            }
        }

        if(curr.key != v)
            return  false;

        //reporting insertion
        int lastLevel = level + 1;
        if(pred.next[lastLevel].isMarked())
        {
            ReportDelete(pred.next[lastLevel].getReference());
            return false;
        }

        ReportInsert(curr,pred.next[lastLevel].isMarked());

        return true;
    }

    @Override
    public Iterator<T> takeSnapshot() {
        ISnapCollector<T> sc = AcquireSnapCollector();
        CollectSnapshot(sc);



        HashMap<T,Boolean> map = new HashMap<T,Boolean>();


       for (T node: sc.ReadPointers()) {
            map.put(node,false);
        }

        for (Report<T> report : sc.ReadReports()) {

            if(map.getOrDefault(report.GetNode(),false))
                map.put(report.GetNode(),false);
            else
                map.put(report.GetNode(),report.GetReportType() == ReportType.DELETED);
        }


        return map.entrySet()
                .stream()
                .filter(e -> !e.getValue())
                .map(e -> e.getKey())
                .iterator();
    }


    private int randomLevel() {
        long random = (long) (Math.random() * Math.pow(2, MAX_LEVEL));
        return (int) (MAX_LEVEL - Math.log10(random) / Math.log10(2));
    }





}
