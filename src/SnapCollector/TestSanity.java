package SnapCollector;

import sun.nio.ch.ThreadPool;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;
import static java.util.Collections.shuffle;

public class TestSanity<T> {

    private IterableList<T> ds;
    private Logger logger;
    private Callable<T> nextItem;
    private int numOfThreads;
    private final int prefill = 5;
    private ExecutorService executor;

    public TestSanity(IterableList<T> ds, Callable<T> nextItem, Logger logger, int numOfThreads) {
        this.ds = ds;
        this.nextItem = nextItem;
        this.logger = logger;
        this.numOfThreads = numOfThreads;
    }

    public void run() throws Exception {
        logger.info("Staring sanity run with prefill " + prefill + " and number of thread " + numOfThreads + " for ds " + ds.getClass().getCanonicalName());
        // generate random elements
        List<T> items = generatePrefill();
        shuffle(items);
        logger.info("1. Verify contains");
        // 1. verify that all items were added to the list
        validateContains(items);
        logger.info("1. Verify contains done");
        shuffle(items);

        // 2. delete half of the items
        logger.info("2. Verify delete half items");
        List<T> itemsDeleted = validateDelete(items, items.size() / 2);
        shuffle(items);

        // validate contains against the list without the deleted items
        List<T> itemsRemained = new LinkedList<>();
        for (T n : items) {
            if (!itemsDeleted.contains(n)) {
                itemsRemained.add(n);
            }
        }
        shuffle(itemsRemained);
        validateContains(itemsRemained);
        logger.info("2. Verify delete half items done");
        shuffle(itemsRemained);

        // 3. add all remained items again and make sure nothing is really added
        logger.info("3. Verify existing items cannot be re-inserted");
        validateExists(itemsRemained);
        logger.info("3. Verify existing items cannot be re-inserted done");
        shuffle(itemsRemained);

        // 4. delete all remained items
        logger.info("3. Verify deleting all items");
        validateDelete(itemsRemained, itemsRemained.size());
        validateEmpty();
        logger.info("3. Verify deleting all items done");

        logger.info("SANITY PASS for " + ds.getClass().getCanonicalName());
    }

    private ExecutorService createExecutorService() {
        if (numOfThreads == 0) return null;
        return Executors.newFixedThreadPool(numOfThreads);
    }

    private void shutdownExecutorService() throws InterruptedException {
        if (executor != null) {
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        }
    }

    private List<T> generatePrefill() throws Exception {
        executor = createExecutorService();
        List<T> items = new LinkedList<>();
        for (int i = 0; i < prefill ; ++i) {
            T n = nextItem.call();
            items.add(n);
            if (executor != null) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        ds.add(n);
                    }
                });
            } else {
                ds.add(n);
            }
        }
        shutdownExecutorService();
        return items;
    }

    private void validateEmpty() {
        int size = 0;

        Iterator<T> iterator = this.ds.takeSnapshot();

        if (iterator == null) {
            logger.info("Data structure " + ds.getClass().getCanonicalName() + " doesn't support iteration, skipping");
            return;
        }


        while (iterator.hasNext())
        {
            size++;
            T t = iterator.next();
        }

        if (size > 0) {
            throw new AssertionError("DS should be empty but it has size " + size);
        }
    }

    private void validateExists(List<T> items) throws InterruptedException {
        executor = createExecutorService();
        for (T n : items) {
            if (executor != null) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (ds.add(n)) {
                            throw new AssertionError("Item " + n + " should already be there");
                        }
                    }
                });
            } else {
                if (this.ds.add(n)) {
                    throw new AssertionError("Item " + n + " should already be there");
                }
            }
        }
        shutdownExecutorService();
    }

    private void validateContains(List<T> items) throws InterruptedException {
        executor = createExecutorService();
        for (T n : items) {
            if (executor != null) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (!ds.contains(n)) {
                            throw new AssertionError("Item " + n + " should appear in DS");
                        }
                    }
                });
            } else {
                if (!ds.contains(n)) {
                    throw new AssertionError("Item " + n + " should appear in DS");
                }
            }
        }
        shutdownExecutorService();
    }

    private List<T> validateDelete(List<T> items, int numToDelete) throws InterruptedException {
        executor = createExecutorService();
        List<T> itemsDeleted = new LinkedList<>();
        int deletedItems = 0;
        for (T n : items) {
            if (deletedItems >= numToDelete) {
                break;
            }
            if (executor != null) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        ds.remove(n);
                    }
                });
            } else {
                ds.remove(n);
            }
            itemsDeleted.add(n);
        }
        shutdownExecutorService();
        return itemsDeleted;
    }
}
