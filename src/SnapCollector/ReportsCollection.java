package SnapCollector;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class ReportsCollection<T> implements IReportsCollection<T> {

    private AtomicReference<Node<T>> head;
    private AtomicReference<Node<T>> tail;
    private Node<T> blocker;

    public ReportsCollection() {
        Node<T> sentinel = new Node<T>(null);
        blocker = new Node<T>(null);

        head = new AtomicReference<>(sentinel);
        tail = new AtomicReference<>(sentinel);
    }

    private void AddNode(Node<T> node) {
        while (true) {

            Node<T> tail = this.tail.get();

            if (tail == blocker) {
                break;
            }

            if (tail.next.compareAndSet(null, node)) {
                this.tail.compareAndSet(tail, node);
                break;
            }
        }
    }

    @Override
    public void AddReport(Report<T> report) {
        Node<T> node = new Node(report);
        AddNode(node);
    }

    @Override
    public void BlockFurtherReports() {
        AddNode(blocker);
    }

    @Override
    public Iterator<Report<T>> iterator() {
        InternalIterator<T> iterator = new InternalIterator();
        iterator.current = head.get();
        return iterator;
    }

    private final class InternalIterator<T> implements Iterator<Report<T>>
    {
        public Node<T> current;

        @Override
        public boolean hasNext() {
            return current.next.get() != null;
        }

        @Override
        public Report<T> next() {
            Node<T> next = current.next.get();
            current = next;
            return next.value;
        }
    }

    public static final class Node<T> {
        final Report<T> value;

        public AtomicReference<Node<T>> next = null;

        public Node(Report<T> x) {
            value = x;
            next = new AtomicReference<>(null);
        }
    }
}


