package SnapCollector;

import javafx.util.Pair;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

public class TestSuite {
    private final String name;
    private final List<Pair<Integer, Integer>> keysRange;
    private final Logger logger;
    private final List<Callable<IterableList<Integer>>> dsList;
    private final int maxThreads;
    private final int numOfIterators;
    private long runningID;
    private final long timeLimitMs;

    public TestSuite(String name, List<Pair<Integer, Integer>> keysRange, int maxThreads, List<Callable<IterableList<Integer>>> dsList, long timeForEachDsMs,int numOfIterators, Logger logger) {
        this.name = name;
        this.keysRange = keysRange;
        this.logger = logger;
        this.dsList = dsList;
        this.maxThreads = maxThreads;
        this.runningID = 0;
        this.timeLimitMs = timeForEachDsMs;
        this.numOfIterators = numOfIterators;
    }

    private void log(String message) {
        logger.info("TestSuite " + name + ": " + message);
    }

    public void start() {

        log("Start with " + dsList.size() + " data structures, " + keysRange.size() + " key ranges, iterators "
                + numOfIterators + "  and " + maxThreads + " max threads");

        // perform test cases
        for (Pair<Integer, Integer> keys : keysRange) {
            for (int i = 1 ; i <= maxThreads ; ++i) {
                for (Callable<IterableList<Integer>> fac : dsList) {
                    IterableList<Integer> ds = generatePrefill(keys,fac);
                    TestCaseConfiguration config = new TestCaseConfiguration(numOfIterators, i, keys.getKey(), keys.getValue());
                    TestCase tc = new TestCase(++runningID, ds, timeLimitMs, config, logger);
                    try {
                        tc.run();
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.exit(1);
                    }
                }
            }
        }
        log("Done at id " + runningID);
    }

    private IterableList<Integer> generatePrefill(Pair<Integer,Integer> range,Callable<IterableList<Integer>> factory) {

        int prefill = range.getValue() / 2;

        IterableList<Integer> ds = null;
        try {
            ds = factory.call();
        }catch (Exception ex)
        {

        }

        for (int i = 0 ; i < prefill ; i++) {
            ds.add(ThreadLocalRandom.current().nextInt(range.getKey(),range.getValue() + 1));
        }

        return ds;
    }
}
