package SnapCollector;

public enum ReportType
{
    INSERTED,
    DELETED
}
