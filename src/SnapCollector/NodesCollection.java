package SnapCollector;


import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class NodesCollection<T>  implements INodesCollection<T>{

    private AtomicReference<Node<T>> tail = null;
    private AtomicReference<Node<T>> head = null;


    private Node<T> blocker;

    public NodesCollection()
    {
        Node<T> sentinel = new Node<>(null);

        head = new AtomicReference<>(sentinel);
        tail = new AtomicReference<>(sentinel);

        blocker = new Node<T>(Integer.MAX_VALUE);
    }

    @Override
    public void AddNode(T value) {
        Node<T> node = new Node<>(value);
        Node<T> tail;
        Node<T> next;

        for(;;) {
            tail = this.tail.get();
            next = tail.next.get();

            if (value == null) throw new AssertionError();

            if(tail.key >= value.hashCode())
                return;

            if(tail != this.tail.get())
                continue;

            if(next == null)
            {
                if(tail.next.compareAndSet(next,node))
                    break;
            }
            else{
                this.tail.compareAndSet(tail,next);
            }
        }

        this.tail.compareAndSet(tail,node);

    }

    @Override
    public void BlockFurtherNodes() {
        tail.set(blocker);
    }

    @Override
    public Iterator<T> iterator() {
        InternalIterator<T> iterator=   new InternalIterator();
        iterator.current = head.get();
        return  iterator;
    }

    private static final class InternalIterator<T> implements Iterator<T>
    {
        public Node<T> current;

        @Override
        public boolean hasNext() {
            return current.next.get() != null;
        }

        @Override
        public T next() {
            Node<T> next = current.next.get();
            current = next;
            return next.value;
        }
    }

    public static final class Node<T> {
        final T value;
        int key = 0;

        public AtomicReference<Node<T>> next = null;

        public Node(int key) {
            value = null;
            this.key = key;
        }

        public Node(T x) {
            value = x;
            next = new AtomicReference<>(null);
            if(x != null)
                key = x.hashCode();
        }
    }
}
