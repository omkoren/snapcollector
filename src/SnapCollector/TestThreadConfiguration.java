package SnapCollector;

public class TestThreadConfiguration<T> {
    public final IterableList<T> ds;

    public TestThreadConfiguration(IterableList<T> ds) {
        this.ds = ds;
    }
}
