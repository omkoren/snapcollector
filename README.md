# SnapCollector Iterator

Implementation of the SnapCollector iterator introdocued in the paper *Lock-Free Data-Structure Iterators* at <http://www.cs.technion.ac.il/~erez/Papers/iterators-disc13.pdf>

The implementation introduced in the repository was developed under Java 1.8

To run the tests see: [Tests -> Running](#markdown-header-running)

* [Data Structures](#markdown-header-data-structures)
    * [Adapters](#markdown-header-adapters)
* [SnapCollector](#markdown-header-snapcollector)
    * [ReportsCollection](#markdown-header-reportscollection)
    * [NodesCollection](#markdown-header-nodescollection)
    * [Integrations](#markdown-header-integrations)
* [Tests](#tests)
    * [Test Threads](#markdown-header-test-threads)
        * [TestThreadWorker](#markdown-header-testthreadworker)
        * [TestThreadActionIterate](#markdown-header-testthreadactioniterate)
    * [TestCase](#markdown-header-testcase)
    * [TestSuite](#markdown-header-testsuite)
    * [Test Results](#markdown-header-test-results)
        * [Parsing the results](#markdown-header-parsing-the-results)
    * [Running](#markdown-header-running)

## Data Structures

We propose the the following data structures:

* `NonBlockingLinkedList` based on the implementation introduced in the book "The Art of Multiprocessor Programming" by Maurice Herlihy and Nir Shavit. (9.8)
* `LockFreeSkipList` based on the implementation introduced in the book "The Art of Multiprocessor Programming" by Maurice Herlihy and Nir Shavit. (14.4)
* `ConcurrentSkipListMap` based on the Java code implementation by Doug Lea integrated with SnapCollector iterator
* `IterableLockFreeSkipList` - a version of `NonBlockingLinkedList` integrated with the SnapCollector iterator
* `IterableNonBlockingLinkedList` - a version of `LockFreeSkipList` integrated with the SnapCollector iterator

Data structures that were integrated with the SnapCollector iterator, was unified under the same interface `IterableList<T>` as follows:

```java
public interface IterableList<T>  {
    boolean add(T x); //add item to the data structures
    boolean remove (T x); //remove item from the data structures
    boolean contains(T x); //check existance of item in the data structures
    Iterator<T> takeSnapshot(); // take a snapshot based on the SnapCollector iterator, returning an Iterator<T> to iterate the items
}
```

### Adapters

Implementation of `ConcurrentSkipListMap<K,T>` carries its own interfaces implementation and set of methods. It also supports custom key type. In order to make it complies with the `IterableList<T>` we introduced two adapters that implement `IterableList<T>` and use `T.hashCode()` (Integer) as the key for the skiplist.

* `ConcurrentSkipListMapAdapter<T>` - adapter for the customized implementation of `ConcurrentSkipListMap<K,T>` with the addition of the SnapCollector iterator
* `ConcurrentNativeSkipListMapAdapter<T>` - adapter for the original Java implementation (`java.util.concurrent.ConcurrentSkipListMap`).

## SnapCollector

The `ISnapCollector<T>` interface represents the methods required by the different data structures in order to integrate the SnapCollector iterator:

```java
public interface ISnapCollector<TNode> {

    void Deactivate(); //Deactivate the collection of snapshot
    boolean IsActive(); //True if collection of snapshot currently active else False
    void Report(Report<TNode> report); //Report an inseration or deletion of node
    void AddNode(TNode node); // Adding node to the snapshot
    void BlockFurtherReports(); //Disallow further reports
    void BlockFurtherNodes(); //Disallow further nodes
    Iterable<Report<TNode>> ReadReports(); //Returns iterator with all reports collected
    Iterable<TNode> ReadPointers(); //Returns iterator with all pointers to node collected
}
```

The interface has a single implementation `SnapCollector<T>` and uses two internal interfaces to maintain the collection of nodes and the collection of reports:

* `IReportsCollection<T>`
* `INodesCollection<T>`

The SnapCollector implementation allocates an instance of `ReportsCollection<T>` for each thread (ThreadLocal) where for `NodesCollection<T>` it allocates a shared instance for all threads to use.

### ReportsCollection

ReportsCollection is represented by the following interface:

```java
public interface IReportsCollection<T> extends Iterable<Report<T>> {
    void AddReport(Report<T> report); //Adding a report to the collection
    void BlockFurtherReports(); //Ignore further report that will be added
}


public class Report<T> {
    private final T _node;
    private final ReportType _reportType;
}

public enum ReportType
{
    INSERTED,
    DELETED
}
```

The implementation introduced in the `ReportsCollection<T>` class is a simple linked-list implementation.   
The `BlockFurtherReports` is implemented by adding a special node to the tail, marking that we don't allow any further insertion.   
The blocking of the list can happen from other threads as well so the paper's suggestion is to simply CAS the tail when inserting.

### NodesCollection

NodesCollection is represented by the following interface:

```java
public interface INodesCollection<T> extends Iterable<T>
{
    void AddNode(T node); //Adding a node to the collection
    void BlockFurtherNodes(); //Ignore further nodes that will be added
}
```

The implementation introduced in the `NodesCollection<T>` class is based on the lock-free queue of Michael and Scott.    
Following the paper, we added an optimization that alters `AddNode` semantics.    
We fail to add any node whose key smaller than or equal to the key of the last node added, this is possible due to the observation that nodes should be added to the snapshot in an ascending order of keys.

## Integrations

The integration to the different data structures involved similar methods/logic, each of them required an internal access to the data structure so it was not possible to generalize their implementation, and it was just implemented in each of the integrated data structures:

Following the paper we have four of methods to add:

* `ReportInsert(newNode,marked)` - Reports an insertion of node with its mark
* `ReportDelete(victim)` - Reports a deletion of "victim" node
* `AcquireSnapCollector()` - Getting or creating an active `SnapCollector`
* `CollectSnapshot(snapCollector)` - Starts collecting nodes from the data structures to a snapCollector instance.

## Tests

The tests were composed from the following hierarchy:   
`TestSuite`s run multiple `TestCase`s which run multiple `TestThread`s.
Each one is explained in the next sections.

### Test Threads

We introduced two type of threads to perform the tests

#### TestThreadWorker

Given a data structure of type `IterableList<T>` the  `TestThreadWorker` thread invoke `add/remove/contains` operations in the ratio of:

* 50% contains
* 25% remove
* 25% add

The thread keeps running until it signaled to stop.

#### TestThreadActionIterate

Given a data structure of type `IterableList<T>` the `TestThreadActionIterate` thread invoke the `takeSnapshot()` and iterates over the items of the result `Iterator<T>`.

The thread keeps running until it signaled to stop.

### TestCase

The `TestCase` class setups a test with the following configurations:

* `IterableList<T>` - Instance of data structure to test.
* Duration - The total time to run the test
* Worker Threads - The number of threads perform `add/remove/contains` on the data structure.
* Iterate Threads - The number of threads perform iterations using `takeSnapshot()` on the data structure.
* Items Randomizer - A delegate to randomize the next item (for workers threads)

When running a test case using the `run()` method it will:

* Setup all threads - Worker Threads and Iteration Threads
* Run all threads
* Wait the given duration time
* Stop the threads
* Collect results from threads
* Write results to file

### TestSuite

The `TestSuite` class setups a test with the following configurations:

* Factory delegates set to initialize instances of `IterableList<T>`, the data structures to test.
* Max Threads - The number of the maximum worker threads that will run concurrently
* Iterators Number - The number of iteration threads to run concurrently in each test case
* Test Case Duration - The duration time for each test case instance
* Key Ranges - A set of key ranges to test (e.g [1,32] , [1,128] )

When running a test suite using the `start()` method it runs a test case for each key range and for each number of threads from 1 to Max Threads and for each factory of `IterableList<T>`.

It initializes a test case by calling the factory delegate and then **prefilling** the data structure that was generated.

***Example:*** If we have 2 factories, 3 ranges and a max threads of 8. The test suite will perform `2 * 3 * 8 = 48` test cases.
Each of them with the given duration and iterator numbers for the test suite.

Prefilling is done by the the following rule:
>If random operations are in the proportions i% insertions, d% deletions, and s% searches on keys drawn uniformly randomly from a key range of size r, then the expected size of the data structure in steady state will be ri/(i+d).

Since our factors are 25% insertions, 25% deletions, 50% searches, we simply prefill the data structure with half of the key range size.

### Test Results

Each `TestCase` outputs the results of the case to a CSV file containing the raw data from all the threads ran in the case.

The file name of the result follows the pattern of `TestCase_{id}_results_{yyyyMMdd}_{HHmmss}.csv`.   
Each line represent results of a thread that ran in the case except of the first line that describes the headers for the columns.   
The list of the columns is as follows (left to right):

* TestCase ID
* Action - The type of the thread ITERATE (iterations thread) or MIXED (worker thread)
* Start Time - Time the thread start work
* End Time - Time the thread end the work
* Total Failures - Total operations that returned `false` (contains,add,remove,takeSnapshot)
* Total Successes - Total operations that returned `true` (contains,add,remove,takeSnapshot)
* Contains Successes - `contains` operations that returned `true`
* Inserts Successes - `add` operations that returned `true`
* Delete Successes - `remove` operations that returned `true`
* Contains Failures - `contains` operations that returned `false`
* Inserts Failures - `add` operations that returned `false`
* Delete Failures - `remove` operations that returned `false`
* Total Operations - Total operation executed by the thread
* Contains Percent - Ratio of `contains` operations
* Inserts Percent - Ratio of `add` operations
* Deletes Percent - Ratio of `remove` operations
* Number Of Threads - Total threads that participate in the test case (same for all lines)
* Lower Key - Lower bound of the key range for the test case (same for all lines) 
* Upper Key - Upper bound of the key range for the test case (same for all lines)
* Data Structure - Name of the data structure that the thread tested (same for all lines)
* Number of Items - The total items that the thread iterated (for ITERATE action only)

#### Parsing the results

We used two tools to build from the raw results described above, tables that reproduce the results showed in the paper.

* Python tool located in: [/scripts/parser.py](/scripts/parser.py)
* .NET tool located in: [/scripts/dotnet-parser/src/ConsoleApp2/Program.cs](/scripts/dotnet-parser/src/ConsoleApp2/Program.cs)

### Running

To run the test program you should first build the program.

Simply run `build.bat`

After building the program you can run it with:

```batch
> SnapCollector.bat

SnapCollector [options...]
 --duration N : duration in ms for a single test case
 --repeat N   : number of times to repeat the test suite
 --threads N  : number of max threads to run the test suite with

Example: SnapCollector --duration N --repeat N --threads N
```

The run will test all the supported data structures (NonBlockingLinkedList,ConcurrentSkipListMap,LockFreeSkipList) with 1 iteration thread and none iteration threads with 3 key ranges ([1,32],[1,128],[1,1024]).

All tests results will be written in a `tests` directory that will be created on the current directory.

All logs will be written both to the console and to a `logs` directory that will be created in the current directory as well