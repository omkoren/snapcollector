﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp2
{

    class Experiment
    {
        public int Upper { get; set; }
        public int Threads { get; set; }
        public string DataStructure { get; set; }
        public long Operations { get; set; }
        public long Iterations { get; set; }
        public long SuccessOperations { get; internal set; }
    }


    public class Program
    {
        public static void Main(string[] args)
        {
            ParseRaw();
           // ParseAgg();
        }


        public static void ParseRaw()
        {
            //var dir = @"D:\Personal\Documents\TAU2\SemesterB_2017\advanced topics\snapcollector\runs\run 2\tests";
            //var dir = @"D:\Personal\Documents\TAU2\SemesterB_2017\advanced topics\snapcollector\tests";
            //var dir = @"D:\Personal\Documents\TAU2\SemesterB_2017\advanced topics\snapcollector\runs\run 2\tests\first runs";

            var dir = @"D:\Personal\Documents\TAU2\SemesterB_2017\advanced topics\snapcollector\runs\run 3\tests";

            var exps = new List<Experiment>();

            foreach (var f in Directory.GetFiles(dir, "*.csv"))
            {
                var lines = File.ReadAllLines(f).Select(x => x.Split(',').Select(y => y.Trim()).ToArray()).ToList();

                var mixed = lines.Where(x => x[1] == "MIXED").ToList();
                var iterate = lines.FirstOrDefault(x => x[1] == "ITERATE");

                var ds = mixed.First()[19].Trim();
                var upper = int.Parse(mixed.First()[18].Trim());
                var operations = mixed.Sum(x => long.Parse(x[12]));
                var success = mixed.Sum(x => long.Parse(x[5]));

                exps.Add(new Experiment
                {
                    DataStructure = ds,
                    Iterations = iterate?[12] == null ? 0 : long.Parse(iterate[12]),
                    Operations = operations,
                    SuccessOperations = success,
                    Threads = mixed.Count,
                    Upper = upper

                });
            }

            var byRange = exps.GroupBy(x => x.Upper)
                 .ToDictionary(x => x.Key, x => x.GroupBy(y => y.Threads)
                                                 .ToDictionary(y => y.Key, y => y.ToList()));



            //Operations

            foreach (var r in byRange)
            {
                using (var fw = File.CreateText("d:\\all_operations" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,ConcurrentSkiplListMap,LockFreeSkipList");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {

                        fw.WriteLine(v.Key + "," 
                                            + v.Value.Where(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Average(x => x.Operations) 
                                            + "," 
                                            +v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Average(x => x.Operations)
                                            + ","
                                            + v.Value.Where(x => x.DataStructure == "SnapCollector.IterableLockFreeSkipList").Average(x => x.Operations)
                                            );


                    }


                }


                using (var fw = File.CreateText("d:\\success_operations" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,ConcurrentSkiplListMap,LockFreeSkipList");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {

                        fw.WriteLine(v.Key + ","
                                            + v.Value.Where(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Average(x => x.SuccessOperations)
                                            + ","
                                            + v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Average(x => x.SuccessOperations)
                                            + ","
                                            + v.Value.Where(x => x.DataStructure == "SnapCollector.IterableLockFreeSkipList").Average(x => x.SuccessOperations)
                                            );


                    }


                }
            }

            //Iterations

            foreach (var r in byRange)
            {
                using (var fw = File.CreateText("d:\\iterations" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,ConcurrentKipListMap,LockFreeSkiplist");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {

                        fw.WriteLine(v.Key + "," 
                                        + v.Value.Where(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Average(x => x.Iterations) + ","
                                                   + v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Average(x => x.Iterations)
                                                   + ","
                                            + v.Value.Where(x => x.DataStructure == "SnapCollector.IterableLockFreeSkipList").Average(x => x.Iterations)
                                                   );


                    }
                }

            }



            //Overhead

            foreach (var r in byRange)
            {
                using (var fw = File.CreateText("d:\\all_overhead" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,ConcurrentSkipListMap,SkipList");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {

                        var avg1 = v.Value.Where(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Average(x => x.Operations);
                        var avg2 = v.Value.Where(x => x.DataStructure == "SnapCollector.NonBlockingLinkedList").Average(x => x.Operations);

                        var overhead_list = avg1 / avg2;
                            
                       

                        var overhead_skiplist =
                            
                            (double)v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Min(x => x.Operations) /
                                               v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentNativeSkipListMapAdapter").Max(x => x.Operations);


                        var overhead_skiplist2 =

                            (double)v.Value.Where(x => x.DataStructure == "SnapCollector.IterableLockFreeSkipList").Min(x => x.Operations) /
                                               v.Value.Where(x => x.DataStructure == "SnapCollector.LockFreeSkipList").Max(x => x.Operations);



                        overhead_list = Math.Round(overhead_list, 2);

                        overhead_skiplist2 = Math.Round(overhead_skiplist2, 2);
                        overhead_skiplist = Math.Round(overhead_skiplist, 2);

                        fw.WriteLine(v.Key + "," + overhead_list + "," + overhead_skiplist + "," + overhead_skiplist2);


                    }


                }


                using (var fw = File.CreateText("d:\\success_overhead" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,ConcurrentSkipListMap,SkipList");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {

                        var avg1 = v.Value.Where(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Average(x => x.SuccessOperations);
                        var avg2 = v.Value.Where(x => x.DataStructure == "SnapCollector.NonBlockingLinkedList").Average(x => x.SuccessOperations);

                        var overhead_list = avg1 / avg2;



                        var overhead_skiplist =

                            (double)v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Min(x => x.SuccessOperations) /
                                               v.Value.Where(x => x.DataStructure == "SnapCollector.ConcurrentNativeSkipListMapAdapter").Max(x => x.SuccessOperations);


                        var overhead_skiplist2 =

                            (double)v.Value.Where(x => x.DataStructure == "SnapCollector.IterableLockFreeSkipList").Min(x => x.SuccessOperations) /
                                               v.Value.Where(x => x.DataStructure == "SnapCollector.LockFreeSkipList").Max(x => x.SuccessOperations);



                        overhead_list = Math.Round(overhead_list, 2);

                        overhead_skiplist2 = Math.Round(overhead_skiplist2, 2);
                        overhead_skiplist = Math.Round(overhead_skiplist, 2);

                        fw.WriteLine(v.Key + "," + overhead_list + "," + overhead_skiplist + "," + overhead_skiplist2);


                    }


                }
            }

        }


        public static void ParseAgg()
        {

            var dir = @"D:\Personal\Documents\TAU2\SemesterB_2017\advanced topics\snapcollector\scripts\results";

            var exps = new List<Experiment>();

            foreach(var f in Directory.GetFiles(dir,"*.csv"))
            {
                var lines = File.ReadAllLines(f).Select(x => x.Split(',').Select(y => y.Trim()).ToArray()).ToList();

                var mixed = lines.FirstOrDefault(x => x[0] == "MIXED");
                var iterate = lines.FirstOrDefault(x => x[0] == "ITERATE");

                exps.Add(new Experiment
                {
                    DataStructure = mixed[3].Trim(),
                    Iterations = iterate?[4] == null ? 0 : (int)double.Parse(iterate[4]),
                    Operations = mixed?[4] == null ? 0 : (int)double.Parse(mixed[4]),
                    Threads = int.Parse(mixed[1]),
                    Upper = int.Parse(mixed[2])

                });
            }

            var byRange = exps.GroupBy(x => x.Upper)
                 .ToDictionary(x => x.Key, x => x.GroupBy(y => y.Threads)
                                                 .ToDictionary(y => y.Key, y => y.ToList()));



            //Operations
            
            foreach (var r in byRange)
            {
                using (var fw = File.CreateText("d:\\operations" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,Skiplist");
                    foreach(var v in r.Value.OrderBy(x => x.Key))
                    {

                        fw.WriteLine(v.Key + "," + v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Operations + "," +
                                                    v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Operations);


                    }


                }
                
            }

            //Iterations

            foreach (var r in byRange)
            {
                using (var fw = File.CreateText("d:\\iterations" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,Skiplist");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {

                        fw.WriteLine(v.Key + "," + v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Iterations + "," +
                                                    v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Iterations);


                    }


                }

            }



            //Overhead

            foreach (var r in byRange)
            {
                using (var fw = File.CreateText("d:\\overhead_agg_" + r.Key + ".csv"))
                {
                    fw.WriteLine("Threads,LinkedList,Skiplist");
                    foreach (var v in r.Value.OrderBy(x => x.Key))
                    {
                        var overhead_list = (double)v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.IterableNonBlockingLinkedList").Operations /
                                            v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.NonBlockingLinkedList").Operations;

                        var overhead_skiplist = (double)v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.ConcurrentSkipListMapAdapter").Operations /
                                                v.Value.FirstOrDefault(x => x.DataStructure == "SnapCollector.ConcurrentNativeSkipListMapAdapter").Operations;

                        overhead_list = Math.Round(overhead_list, 2);
                        overhead_skiplist = Math.Round(overhead_skiplist, 2);

                        fw.WriteLine(v.Key + "," + overhead_list + "," + overhead_skiplist);


                    }


                }

            }



        }
    }
}
