import sys
import os

class TestConfig:
    def __init__(self, action, threads, upper_key, ds):
        self.action = action
        self.threads = threads
        self.upper_key = upper_key
        self.ds = ds

    def __str__(self):
        return "test config: action {0}, threads {1}, key {2}, ds {3}".format(self.action, self.threads, self.upper_key, self.ds)

    def __hash__(self):
        return hash((self.action, self.threads, self.upper_key, self.ds))

    def __eq__(self, other):
        return (self.action, self.threads, self.upper_key, self.ds) == (other.action, other.threads, other.upper_key, other.ds)

    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not(self == other)


def parse_single_file(filename, tests_results):
    local_results = {}
    with open(filename, 'r') as f:
        headers = []
        lines = f.readlines()
        for i in xrange(len(lines)):
            line = [l.strip() for l in lines[i].split(',')[:-1]]
            if i == 0:
                headers = line
                continue
            action = ""
            threads = 0
            upper_key = 0
            total_ops = 0
            ds = ""
            for j in xrange(len(line)):
                if headers[j] == "Action":
                    action = line[j]
                elif headers[j] == "Number Of Threads":
                    threads = int(line[j])
                elif headers[j] == "Upper Key":
                    upper_key = int(line[j])
                elif headers[j] == "Data Structure":
                    ds = line[j]
                elif headers[j] == "Total Operations":
                    total_ops = int(line[j])
            # add to local results
            test_config = TestConfig(action, threads, upper_key, ds)
            if test_config not in local_results:
                local_results[test_config] = 0
            local_results[test_config] += total_ops

    # add local results to tests_results
    for test_config in local_results.keys():
        if test_config not in tests_results:
            tests_results[test_config] = []
        tests_results[test_config].append(local_results[test_config])


def write_tests_results(tests_results):
    all_ds = set([ test_config.ds for test_config in tests_results.keys() ])
    all_threads = set([ test_config.threads for test_config in tests_results.keys() ])
    all_keys = set([test_config.upper_key for test_config in tests_results.keys()])

    for ds in all_ds:
        for thread in all_threads:
            for key in all_keys:
                print "{0} {1} {2}".format(ds, thread, key)
                filename = os.path.join("results", "TestResult_{0}_threads_{1}_key_{2}.csv".format(ds, thread, key))
                all_test_config = [test_config for test_config in tests_results.keys() if test_config.ds == ds and
                                   test_config.threads == thread and test_config.upper_key == key]
                if len(all_test_config) == 0:
                    continue
                with open(filename, "w+") as f:
                    f.writelines("Action, Number Of Threads, Upper Key, Data Structure, Total Operations\n")
                    for test_config in all_test_config:
                        avg = float(sum(tests_results[test_config])) / float(len(tests_results[test_config]))
                        f.writelines("{0}, {1}, {2}, {3}, {4}\n".format(test_config.action, test_config.threads,
                                                                        test_config.upper_key, test_config.ds, avg))


def write_overhead_results(tests_results):
    all_ds = set([ test_config.ds for test_config in tests_results.keys() ])
    all_threads = set([ test_config.threads for test_config in tests_results.keys() ])
    all_keys = set([test_config.upper_key for test_config in tests_results.keys()])
    non_iterable_ds = ["SnapCollector.NonBlockingLinkedList", "SnapCollector.ConcurrentNativeSkipListMapAdapter"]
    iterable_ds = ["SnapCollector.IterableNonBlockingLinkedList", "SnapCollector.ConcurrentSkipListMapAdapter"]
    tests_results_fraction = {}
    for ds in all_ds:
        for thread in all_threads:
            for key in all_keys:
                all_test_config = [test_config for test_config in tests_results.keys() if test_config.ds == ds and
                                   test_config.threads == thread and test_config.upper_key == key]
                if len(all_test_config) == 0:
                    continue
                iterable_test_config = [test_config for test_config in all_test_config if test_config.ds in iterable_ds]
                for test_config in iterable_test_config:
                    if test_config.action == "ITERATE":
                        continue
                    if test_config.ds == "SnapCollector.IterableNonBlockingLinkedList":
                        twin_ds = "SnapCollector.NonBlockingLinkedList"
                    else:
                        twin_ds = "SnapCollector.ConcurrentNativeSkipListMapAdapter"
                    twin_test_config = [ ts for ts in tests_results.keys() if ts.ds == twin_ds
                                         and ts.action == test_config.action and ts.threads == test_config.threads
                                         and ts.upper_key == test_config.upper_key ][0]
                    fraction_test_config = TestConfig(test_config.action, test_config.threads, test_config.upper_key,
                                                      test_config.ds + "/" + twin_test_config.ds)
                    avg = float(sum(tests_results[test_config])) / float(len(tests_results[test_config]))
                    avg_twin = float(sum(tests_results[twin_test_config])) / float(len(tests_results[twin_test_config]))
                    tests_results_fraction[fraction_test_config] = float(avg) / float(avg_twin)

    # write results
    filename = os.path.join("results", "TestResult_overhead_results.csv")
    with open(filename, "w+") as f:
        f.writelines("Action, Number Of Threads, Upper Key, Data Structure, Total Operations\n")
        for test_config in tests_results_fraction:
            avg = tests_results_fraction[test_config]
            f.writelines("{0}, {1}, {2}, {3}, {4}\n".format(test_config.action, test_config.threads,
                                                            test_config.upper_key, test_config.ds, avg))

    import operator
    max_entry = max(tests_results_fraction.iteritems(), key=operator.itemgetter(1))
    print "max fraction from test config {0} value {1}".format( max_entry[0], max_entry[1])







def parse_results(dir):
    tests_results = {}
    files = 0
    for filename in os.listdir(dir):
        if filename.endswith(".csv"):
            parse_single_file(os.path.join(dir, filename), tests_results)
            files = files + 1
#            print filename
    write_tests_results(tests_results)
    write_overhead_results(tests_results)
    print "went over " + str(files) + " files"
    return True


def main():
    if len(sys.argv) != 2:
        print "Must provide directory path"
        return 1

    dir = sys.argv[1]

    if not parse_results(dir):
        print "Parse results failed"
        return 1
    return 0


if __name__ == "__main__":
    sys.exit(main())