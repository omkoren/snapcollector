#!/bin/bash -x
logfile="run_command.log"
echo > $logfile
cmd="java -d64 -server -cp out/production/project SnapCollector.Main"
date=$(date)
echo "Running command at $date, command $cmd" >> $logfile
echo "Starting to run $date, command $cmd"
env LD_PRELOAD=libjemalloc.so numactl --interleave=all $cmd
res=$?
echo "Command returned $res at $date" >> $logfile
echo "Command done with res $res"
if [ $res -eq 0 ] ; then
	echo "SUCCESS"
else
	echo "FAILURE"
fi

